function add(input) {
    if (input === "") {
        return 0;
    }
    let total = 0;
    if (typeof input === "number") return input;
    if (typeof input != "string") return `${typeof input} Type not allowed`;
    const inputArray = input.split(/[\n,;/]/);
    const negArray = [];
    if (inputArray?.length) {
        inputArray.forEach((val)=>{
            let number = parseInt(val);
            if (number < 0) {
                negArray.push(number);
            }
            total += Number(val);
        })
    }

    if (negArray?.length) {
        let message = "Negative number not allowed: ";
        if (negArray?.length > 1) {
            message = "Negative numbers not allowed: " + negArray.join(', ');
        } else {
            message = message + negArray[0];
        }
        return new Error(`${message}`).toString();
    }

    return total;
}

module.exports = add;