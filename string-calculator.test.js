var add = require("./string-calculator");

test('Send empty string and check the result', () => {
    expect(add("")).toBe(0);
});


test('Send a single numbr and check the output which is same as input', () => {
    expect(add("2")).toBe(2);
});

test('Send a single negative numbr and check the output which is same as input', () => {
    expect(add("-3")).toBe("Error: Negative number not allowed: -3");
});

test('Send a single negative numbr and check the output which is same as input', () => {
    expect(add("-3, -4")).toBe("Error: Negative numbers not allowed: -3, -4");
});

test('Add two numbers separated by comma', () => {
    expect(add('1,2')).toEqual(3);
});

test('Add numbers with \n and comma between them', () => {
    expect(add('1\n2,3')).toEqual(6);
});

test('Add numbers with multiple delimiters', () => {
    expect(add('//;\n1;2')).toEqual(3);
});

test('throws exception for negative numbers', () => {
    expect(add('1,-2,3,-4')).toBe("Error: Negative numbers not allowed: -2, -4");
});

test('Trying with n numbers of input', () => {
    expect(add('1,2,3,4,5,6,7,8,9,10')).toEqual(55);
});

test('Trying with n numbers of ,/\n', () => {
    expect(add('1,2,3,4,,,,,5\n,6,7,8,,,,,,//9,10')).toEqual(55);
});

test('Trying with n numbers negative numbers ', () => {
    expect(add('1,2,3,4,,,,,5\n,6,-4,-5,-67,-2,-6,,7,8,,,,,,//9,10')).toBe("Error: Negative numbers not allowed: -4, -5, -67, -2, -6");
});

test('Test case for error message singular', () => {
    expect(add('-10')).toBe("Error: Negative number not allowed: -10");
});

test('Test case for error message plural', () => {
    expect(add('-10\n-6')).toBe("Error: Negative numbers not allowed: -10, -6");
});

test('Test case for error message plural', () => {
    expect(add(9)).toBe(9);
});

test('Invalid input type test', () => {
    expect(add({})).toBe("object Type not allowed");
});
test('Invalid input type test', () => {
    expect(add([])).toBe("object Type not allowed");
});